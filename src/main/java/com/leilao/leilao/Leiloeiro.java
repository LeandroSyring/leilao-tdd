package com.leilao.leilao;

import java.util.List;

public class Leiloeiro {

    private String nome;

    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(){

        List<Lance> lances = leilao.getLances();

        Lance maiorLance = new Lance();

        for (Lance lance:lances) {
            if(lance.getValorDoLance() > maiorLance.getValorDoLance()){
                  maiorLance = lance;
            }
        }
        return maiorLance;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }
}
