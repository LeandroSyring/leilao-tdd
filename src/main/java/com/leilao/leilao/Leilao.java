package com.leilao.leilao;

import java.util.List;



public class Leilao {

    private List<Lance> lances;

    public Leilao(){

    }

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Lance adicionarNovoLance(Lance lance) throws Exception {

        if (validarLance(lance).getValorDoLance() == 0) {
            throw new Exception();
        } else {
            List<Lance> lances = getLances();
            lances.add(lance);
            return lance;
        }
    }

    public Lance validarLance(Lance lance){

        List<Lance> lances = getLances();

        for (Lance lanceFor:lances) {
            if(lanceFor.getValorDoLance() > lance.getValorDoLance()){
                Lance lanceRetorno = new Lance();
                return lanceRetorno;
            }
        }
        return lance;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }
}
