package com.leilao.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTestes {

    @Test
    public void testarValidarLance(){
        Usuario usuario = new Usuario(1,"Leandro Syring");
        Lance lance = new Lance(usuario, 10.1);
        List<Lance> lances = new ArrayList<>();
        lances.add(lance);
        Leilao leilao = new Leilao(lances);
        Assertions.assertEquals(leilao.validarLance(new Lance(usuario, 10)).getValorDoLance(), 0);
    }

    //Valida a inserção de um novo Lance, com valor maior que o último.
    @Test
    public void testarAdicionarNovoLance() throws Exception {
        Usuario usuario = new Usuario(1, "Leandro Syring");
        Lance lance = new Lance(usuario, 10.1);
        List<Lance> lances = new ArrayList<>();
        lances.add(lance);
        Leilao leilao = new Leilao(lances);
        Assertions.assertEquals(leilao.adicionarNovoLance(new Lance(usuario,10.2)).getValorDoLance(),new Lance(usuario,10.2).getValorDoLance());
    }

    //Valida a inserção de um novo Lance, com valor menor que o último
    @Test
    public void testarAdicionarNovoLanceInferior() throws Exception {
        Usuario usuario = new Usuario(1, "Leandro Syring");
        Lance lance = new Lance(usuario, 10.1);
        List<Lance> lances = new ArrayList<>();
        lances.add(lance);
        Leilao leilao = new Leilao(lances);
        //Assertions.assertThrows(leilao.adicionarNovoLance(new Lance(usuario,10)), );
        Assertions.assertThrows(Exception.class, () -> {
            leilao.adicionarNovoLance(new Lance(usuario,10));
        });
    }
}
