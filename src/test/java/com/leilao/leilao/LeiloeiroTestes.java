package com.leilao.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTestes {

    @Test
    public void testarRetornarMaiorLance(){

        Usuario usuario = new Usuario(1, "Leandro Syring");
        Usuario usuario2 = new Usuario(2, "José");
        List<Lance> lances = new ArrayList<>();
        lances.add(new Lance(usuario, 10));
        lances.add(new Lance(usuario2, 32));
        lances.add(new Lance(usuario, 15));
        lances.add(new Lance(usuario2, 31));
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Roberto Leilão",leilao);
        Assertions.assertEquals(leiloeiro.retornarMaiorLance().getValorDoLance(),32);
    }
    @Test
    public void testarRetornarMaiorLanceNomeUsuario(){

        Usuario usuario = new Usuario(1, "Leandro Syring");
        Usuario usuario2 = new Usuario(2, "José");
        List<Lance> lances = new ArrayList<>();
        lances.add(new Lance(usuario, 10));
        lances.add(new Lance(usuario, 15));
        lances.add(new Lance(usuario2, 31));
        lances.add(new Lance(usuario2, 32));
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Roberto Leilão",leilao);
        Assertions.assertEquals(leiloeiro.retornarMaiorLance().getUsuario().getNome(),"José");
    }
}
